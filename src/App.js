import ToDoList from './toDoList';
import ProkasTimer from './prokasTimer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Simple Prokastidoro Timer</h1>
      </header>
      <main>
        <noscript><p>This Timer uses the <a href='github.com/facebook/react'>React Javascript Framework by facebook</a>, was I wanted to learn it. To my knowledge nothing you do here should leave your browser. I will still likely make a second version of this app using <a href='https://svelte.dev/'>Svelte</a>.</p></noscript>
        <ProkasTimer/>
        <h2>To Do:</h2>
        <ToDoList />
      </main>
      <footer>
        <h2>How to use?</h2>
        <p><a href='https://en.wikipedia.org/wiki/Pomodoro_Technique'>Pomodoro</a> is a great technique to improve your productivity. However, some people with ADHD prefer Prokastidoro. </p>
        <p>First, you <b>start with a break</b>. Then try to stay focused for at least 5 Minutes. And if you got into the flow, you might as well keep going.</p>
      </footer>
    </div>
  );
}

export default App;
