import { useState, useId } from 'react';

function CheckItem({checkText, id}) {
  const [checked, setChecked] = useState(false);

  return (
    <div className="CheckItem">
      <input type="checkbox" id={id} /><label for={id}> {checkText} </label>
    </div>
  );
}

export default CheckItem;