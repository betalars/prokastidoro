import { useState, useEffect, useRef } from 'react';
export default ProkasTimer;

function ProkasTimer() {
  const restSeconds = 30*60;
  const focusSeconds = 5*60;
  const [state, setState] = useState(0);
  const [remainingSeconds, setRemainingSeconds] = useState(restSeconds);
  const [elapsedPercent, setElapsedPercent] = useState(100);
  const [instructions, setInstructions] = useState("start with a break");
  const [buttonLabel, setButtonLabel] = useState("Start!");
  const id =useRef(null);
  const clear=()=>{
    window.clearInterval(id.current)
  }
  function buttonPress(){
    if (state === 0 || state === 4){
      setRemainingSeconds(restSeconds)
      setState(1)
    } else {
      setRemainingSeconds(focusSeconds)
      setState(3)
    };
  }

  useEffect(()=>{
    switch(state){
      case 1:
        setInstructions("Break ends in:")
        setButtonLabel("Start Focus!")
        break;
      case 2:
        setInstructions("Break ended!")
        break;
      case 3:
        setInstructions("Focus for:")
        setButtonLabel("Restart Focus")
        break;
      case 4:
        setInstructions("Focus Streak!")
        setButtonLabel("Take a Break")
        break;
    }
  },[state])

  useEffect(()=>{
    if (state != 0 && state != 2){
        id.current=window.setInterval(()=>{
            if (state != 4){
              setRemainingSeconds((remainingSeconds)=>remainingSeconds-1)
            } else
              setRemainingSeconds((remainingSeconds)=>remainingSeconds+1)
          },1000)
          return ()=>clear();
      }
    },[state])

  useEffect(()=>{
    if(remainingSeconds===0){
      if (state === 1){
        clear()
        setState(2)
      }
      if (state === 3){
        setRemainingSeconds(focusSeconds)
        setState(4)
      }
    }

  },[remainingSeconds])

  useEffect(()=>{
    if (state === 1){
      setElapsedPercent((remainingSeconds/restSeconds)*98)
    } else {
      setElapsedPercent((1-(remainingSeconds/focusSeconds))*100)
    }

  },[remainingSeconds])


  return (
    <div className={"clock" + ((state === 1 || state === 2 && remainingSeconds < 120) ? " break": "") + ((state === 3) ? " focus": "")}>
      <svg 
        width="256px"
        height="256px"
        viewBox="0, 0, 256px, 256px"
        aria-hidden="true"
        >
        <circle
          r="118px"
          cx="50%"
          cy="50%"
          fill="#00000000"
          stroke="#49ff55ff"
          stroke-width="10"
          stroke-linecap="round"
          pathLength="100"
          strokeDasharray={elapsedPercent.toString() + " " + (100-elapsedPercent).toString()}
        />
      </svg>
      <div className={"clockface"}>
        <label>{instructions}</label>
        <label className='time'>{("0"+Math.floor(remainingSeconds/60)).slice(-2)}:{("0"+remainingSeconds%60).slice(-2)}</label>
        <button onClick={buttonPress}>{buttonLabel}</button>
      </div>
    </div>
  );
}