import {useState, useEffect} from 'react';
import CheckItem from './checkItem';

function ToDoList() {
  const [taskDescription, setTaskDescription] = useState('');
  const [buttonDisabled, setButtonDisabled] = useState('true')
  useEffect(()=>{
    setButtonDisabled(taskDescription==='')
  },[taskDescription])

  function handleInput(event){
    setTaskDescription(event.target.value)
  }
  function handleKey(event){
    if (event.key === 'Enter')
      spawnTask()
  }

  const initialList = [];
  const [tasks, setTasks] = useState([]);
  function spawnTask(){
    const newList = tasks.concat({name:taskDescription, id:'Task-Item-' + tasks.length});
    setTasks(newList);
    setTaskDescription('');
  }

  const taskList = tasks.map(task =>
      <li key={task.id}><CheckItem checkText={task.name} id={task.id}/></li>
    )

  return (
    <div className="toDoList">
      <div><button onClick={spawnTask} disabled={buttonDisabled}>Add</button><input type='text' placeholder='What do you want to do?' value={taskDescription} onChange={handleInput} onKeyDown={handleKey}/></div>
      <div>
        <ul>
          {taskList}
        </ul>
      </div>
    </div>
  );
}

export default ToDoList;